2.5kV RSR232 (TTL level) Isolator with up to 400mA host supply and FTDI USB interface

http://lemmini.de/RS232%20TTL%20Isolator/RS232%20TTL%20Isolator.html

---------------------------------------------------------------
Copyright 2015 Volker Oth - VolkerOth(at)gmx.de

Licensed under the Creative Commons Attribution 4.0 license
http://creativecommons.org/licenses/by/4.0/

Everything in this repository is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OF ANY KIND, either express or implied.